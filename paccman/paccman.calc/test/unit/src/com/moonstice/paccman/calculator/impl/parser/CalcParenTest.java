/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test class for handling parenthesis keys.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcParenTest extends CalcParserTestBase {

    public CalcParenTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testIgnoreParen() {
        System.out.println("IgnoreParen");

        testStringParse("(=", "0", "0");
        testStringParse("()=", "0", "0", "0");
        testStringParse("(7)=", "0", "7", "7", "7");
    }

    @Test
    public void testParenAroundSingleNumber() {
        System.out.println("ParenAroundSingleNumber");

        testStringParse("(1)=", "0", "1", "1", "1");
        testStringParse("1+()=", "1", "1", "1", "1", "2");
        testStringParse("2x3+(1)=", "2", "2", "3", "6", "6", "1", "1", "7");
        testStringParse("2+3x(5)=", "2", "2", "3", "3", "3", "5", "5", "17");
        testStringParse("2x(3+(1))=", "2", "2", "2", "3", "3", "3", "1", "1", "4", "8");
    }

    @Test
    public void testParenAroundSingleOperation() {
        System.out.println("ParenAroundSingleOperation");

        testStringParse("(1+2)=", "0", "1", "1", "2", "3", "3");
        testStringParse("(7-3)=", "0", "7", "7", "3", "4", "4");
        testStringParse("(3x2)=", "0", "3", "3", "2", "6", "6");
        testStringParse("(6/2)=", "0", "6", "6", "2", "3", "3");
    }

    @Test
    public void testMissingOperandBetweenParen() {
        System.out.println("testMissingOperandAfterParen");

        testStringParse("(+2)=", "0", "0", "2", "2", "2");
        testStringParse("(2+)=", "0", "2", "2", "4", "4");
        testStringParse("(+2+7)=", "0", "0", "2", "2", "7", "9", "9");
        testStringParse("(2+)+3=", "0", "2", "2", "4", "4", "3", "7");
        testStringParse("(2+3+)+7=", "0", "2", "2", "3", "5", "10", "10", "7", "17");
        testStringParse("(2+3x)+7=", "0", "2", "2", "3", "3", "11", "11", "7", "18");
        testStringParse("(2x3+)+7=", "0", "2", "2", "3", "6", "12", "12", "7", "19");
    }

    @Test
    public void testUselessParen() {
        System.out.println("UselessParen");

        testStringParse("(1+2)+6=", "0", "1", "1", "2", "3", "3", "6", "9");
        testStringParse("(2x3)x5=", "0", "2", "2", "3", "6", "6", "5", "30");
    }

    @Test
    public void testParenChangePriority() {
        System.out.println("ParenChangePriority");

        testStringParse("1+(2+5)=", "1", "1", "1", "2", "2", "5", "7", "8");
        testStringParse("2x(3+4)=", "2", "2", "2", "3", "3", "4", "7", "14");
        testStringParse("1+(2+5)+3=", "1", "1", "1", "2", "2", "5", "7", "8", "3", "11");
        testStringParse("2x(3+4)+3=", "2", "2", "2", "3", "3", "4", "7", "14", "3", "17");
        testStringParse("3+(1+2x5)=", "3", "3", "3", "1", "1", "2", "2", "5", "11", "14");
        testStringParse("1+(2+5)x3=", "1", "1", "1", "2", "2", "5", "7", "7", "3", "22");
    }

    @Test
    public void testDoubleParen() {
        System.out.println("DoubleParen");

        testStringParse("((1+2)x5+7)x2=", "0", "0", "1", "1", "2", "3", "3", "5",
                "15", "7", "22", "22", "2", "44");
        testStringParse("(3+(1+4)x8+7)x2=", "0", "3", "3", "3", "1", "1", "4", "5",
                "5", "8", "43", "7", "50", "50", "2", "100");
    }

    @Test
    public void testMultilevelParen() {
        System.out.println("MultilevelParen");

        testStringParse("(1+(2+(5+7)))+1=", "0", "1", "1", "1", "2", "2", "2",
                "5", "5", "7", "12", "14", "15", "15", "1", "16");
    }

    @Test
    public void testEvalCloseAllParen() {
        System.out.println("EvalCloseAllParen");

        testStringParse("(1+2=", "0", "1", "1", "2", "3");
        testStringParse("((1+2=", "0", "0", "1", "1", "2", "3");
        testStringParse("2x(1+(3+7=", "2", "2", "2", "1", "1", "1", "3", "3",
                "7", "22");
        testStringParse("2+(1+(3+7=", "2", "2", "2", "1", "1", "1", "3", "3",
                "7", "13");
        testStringParse("2+(1+(2x3+7=", "2", "2", "2", "1", "1", "1", "2", "2",
                "3", "6", "7", "16");
        testStringParse("5x(2+3=", "5", "5", "5", "2", "2", "3", "25");
        testStringParse("2x(3+(1=", "2", "2", "2", "3", "3", "3", "1", "8");
        testStringParse("5x(2+3=1)2+3=", "5", "5", "5", "2", "2", "3", "25",
                "1", false, "12", "12", "3", "15");
    }

    @Test
    public void testMaxLevelParen() {
        System.out.println("MaxLevelParen");

        {
            CalcParser instance = new CalcParser(DEFAULT_MAX_PRECISION);
            for (int i = 0; i < ExprParser.MAX_PAREN_LEVEL; i++) {
                Assert.assertTrue(instance.parseChar('('));
            }
            Assert.assertFalse(instance.parseChar('('));
        }
        {
            CalcParser instance = new CalcParser(DEFAULT_MAX_PRECISION);
            for (int i = 0; i < ExprParser.MAX_PAREN_LEVEL; i++) {
                Assert.assertTrue(instance.parseChar('('));
            }
            Assert.assertTrue(instance.parseChar(')'));
            Assert.assertTrue(instance.parseChar('+'));
            Assert.assertTrue(instance.parseChar('('));
            Assert.assertFalse(instance.parseChar('('));
        }
    }

    @Test
    public void testParenAfterEval() {
        System.out.println("ParenAfterEval");

        testStringParse("1=(2+3)=", "1", "1", "0", "2", "2", "3", "5", "5");
        testStringParse("1=((2+3)))=", "1", "1", "0", "0", "2", "2", "3", "5", "5", false, "5");
        testStringParse("1=(2+3)=/2=", "1", "1", "0", "2", "2", "3", "5", "5", "5", "2", "2.5");
        testStringParse("1+2=(2+4)=/2=", "1", "1", "2", "3", "0", "2", "2", "4", "6", "6", "6", "2", "3");
        testStringParse("1+2=(2+4=/2=", "1", "1", "2", "3", "0", "2", "2", "4", "6", "6", "2", "3");
    }

    @Test
    public void testBadExprWithParen() {
        System.out.println("BadExprWithParen");

        testStringParse(")=", false, "0");
        testStringParse("=)", "0", false);
        testStringParse("())=", "0", "0", false, "0");
        testStringParse("12(+3=", "1", "12", false, "12", "3", "15");
        testStringParse("12(3+4=", "1", "12", false, "123", "123", "4", "127");
        testStringParse("12)3+4=", "1", "12", false, "123", "123", "4", "127");
        testStringParse("1+2=)5+2x3=", "1", "1", "2", "3", false, "5", "5", "2",
                "2", "3", "11");
        testStringParse("(1+2)34=", "0", "1", "1", "2", "3", false, false, "3");
        testStringParse("(1+2)(=", "0", "1", "1", "2", "3", false, "3");
    }

}
