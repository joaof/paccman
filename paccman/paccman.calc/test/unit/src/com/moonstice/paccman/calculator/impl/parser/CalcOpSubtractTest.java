/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for testing subtract operation.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcOpSubtractTest extends CalcParserTestBase {

    public CalcOpSubtractTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testSubtractTwoOperands() {
        System.out.println("SubstractTwoOperands");

        testStringParse("5-1=", "5", "5", "1", "4");
        testStringParse("12-3=", "1", "12", "12", "3", "9");
        testStringParse("1-35=", "1", "1", "3", "35", "-34");
        testStringParse("45-17=", "4", "45", "45", "1", "17", "28");
        testStringParse("1.2-34=", "1", "1.", "1.2", "1.2", "3", "34", "-32.8");
        testStringParse("1-3.4=", "1", "1", "3", "3.", "3.4", "-2.4");
        testStringParse("1.2-3.7=", "1", "1.", "1.2", "1.2", "3", "3.", "3.7", "-2.5");
    }

    @Test
    public void testSubtractThreeOperands() {
        System.out.println("SubtractThreeOperands");

        testStringParse("12-3-7=", "1", "12", "12", "3", "9", "7", "2");
        testStringParse("52-23-5.7=", "5", "52", "52", "2", "23", "29", "5", "5.", "5.7", "23.3");
        testStringParse("1-2s3-5.7=", "1", "1", "2", "-2", "-23", "24", "5", "5.", "5.7", "18.3");
        testStringParse("71-23-5.7s=", "7", "71", "71", "2", "23", "48", "5", "5.", "5.7", "-5.7", "53.7");
        testStringParse("1-023-5.7s=", "1", "1", "0", "2", "23", "-22", "5", "5.", "5.7", "-5.7", "-16.3");
        testStringParse("1-023-5.70=", "1", "1", "0", "2", "23", "-22", "5", "5.", "5.7", "5.70", "-27.7");
    }

}
