/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for testing divide operation.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcOpDivTest extends CalcParserTestBase {

    public CalcOpDivTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testDivTwoOperands() {
        System.out.println("DivTwoOperands");

        testStringParse("5/2=", "5", "5", "2", "2.5");
        testStringParse("12/3=", "1", "12", "12", "3", "4");
        testStringParse(6, "120/7=", "1", "12", "120", "120", "7", "17.143" /* 17.142857... */);
        testStringParse(5, "6/34=", "6", "6", "3", "34", "0.176" /* 0.17647... */);
        testStringParse(7, "69633/34=", "6", "69", "696", "6963", "69633", "69633", "3", "34", "2048.03" /* 2048.0294117... */);
        testStringParse(4, "45/12=", "4", "45", "45", "1", "12", "3.75");
        testStringParse(6, "17.2/3s4=", "1", "17", "17.", "17.2", "17.2", "3", "-3", "-34", "-0.5059" /* -0.5058823... */);
        testStringParse(4, "1/3.4=", "1", "1", "3", "3.", "3.4", "0.29" /* 0.2941176470 */);
        testStringParse(6, "6.2/3.4=", "6", "6.", "6.2", "6.2", "3", "3.", "3.4", "1.8235" /* 1.8235294... */);
    }

    @Test
    public void testDivThreeOperands() {
        System.out.println("DivThreeOperands");

        testStringParse(5, "2/3/7=", "2", "2", "3", "0.667", "7", "0.095"/* .0952428571 */);
        testStringParse(10, "55/23/5.7=", "5", "55", "55", "2", "23", "2.39130435", "5", "5.", "5.7", "0.41952708");
        testStringParse(10, "4/2s3/5.7=", "4", "4", "2", "-2", "-23", "-0.17391304", "5", "5.", "5.7", "-0.03051106");
        testStringParse(5, "4/23/5.7s=", "4", "4", "2", "23", "0.174", "5", "5.", "5.7", "-5.7", "-0.031");
        testStringParse(5, "4/023/5.7s=", "4", "4", "0", "2", "23", "0.174", "5", "5.", "5.7", "-5.7", "-0.031");
        testStringParse(5, "4/023/5.70=", "4", "4", "0", "2", "23", "0.174", "5", "5.", "5.7", "5.70", "0.031");
    }

}
