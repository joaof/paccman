/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcNumberParseTest extends CalcParserTestBase {

    public CalcNumberParseTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    /**
     * Test of default display (no input from user)
     */
    @Test
    public void testNoEntry() {
        System.out.println("NoEntry");

        CalcParser instance = new CalcParser(6);
        String expResult = "0";
        String result = instance.getDisplay();
        Assert.assertEquals(expResult, result);
    }

    /**
     * Test of one digit entry
     */
    @Test
    public void testDigitEntry() {
        System.out.println("DigitEntry");

        testStringParse("1", "1");
    }

    /**
     * Test of two digits entry
     */
    @Test
    public void testTwoDigitEntry() {
        System.out.println("TwoDigitEntry");

        testStringParse("12", "1", "12");
    }

    /**
     * Test of multi digit entry
     */
    @Test
    public void testTwoOrMoreDigitEntry() {
        System.out.println("TwoOrMoreDigitEntry");

        testStringParse("1234", "1", "12", "123", "1234");
    }

    /**
     * Test ignore of zeroes.
     */
    @Test
    public void testIgnoreLeadingZeroes() {
        System.out.println("IgnoreLeadingZeroes");

        testStringParse("000", "0", "0", "0");
        testStringParse("0123", "0", "1", "12", "123");
        testStringParse("10", "1", "10");
    }

    /**
     * Test one decimal.
     */
    @Test
    public void testOneDecimal() {
        System.out.println("OneDecimal");

        testStringParse(".1", "0.", "0.1");
        testStringParse("0.1", "0", "0.", "0.1");
        testStringParse("3.4", "3", "3.", "3.4");
        testStringParse("1.", "1", "1.");
    }

    /**
     * Test two decimals.
     */
    @Test
    public void testTwoDecimals() {
        System.out.println("TwoDecimals");

        testStringParse(".12", "0.", "0.1", "0.12");
        testStringParse("0.34", "0", "0.", "0.3", "0.34");
        testStringParse("9.87", "9", "9.", "9.8", "9.87");
        testStringParse("1.00", "1", "1.", "1.0", "1.00");
        testStringParse("01.00", "0", "1", "1.", "1.0", "1.00");
    }

    @Test
    public void testBadDecimalPoint() {
        System.out.println("BadDecimalPoint");

        testStringParse("..12", "0.", false, "0.1", "0.12");
        testStringParse("0..12", "0", "0.", false, "0.1", "0.12");
        testStringParse("1..23", "1", "1.", false, "1.2", "1.23");
        testStringParse("1.23.4", "1", "1.", "1.2", "1.23", false, "1.234");
        testStringParse("1...23", "1", "1.", false, false, "1.2", "1.23");
    }

    @Test
    public void testSigned() {
        System.out.println("Signed");

        testStringParse("s", "-0");
        testStringParse("1s", "1", "-1");
        testStringParse("12s", "1", "12", "-12");
        testStringParse("0s", "0", "-0");
        testStringParse("1s2s", "1", "-1", "-12", "12");
        testStringParse("1.2s3", "1", "1.", "1.2", "-1.2", "-1.23");
        testStringParse("1.2s.3", "1", "1.", "1.2", "-1.2", false, "-1.23");
    }

    @Test
    public void testMaxDigits() {
        System.out.println("MaxDigits");

        testStringParse(1, "012", "0", "1", false);
        testStringParse(3, "1111", "1", "11", "111", false);
        testStringParse(3, "1s111", "1", "-1", "-11", "-111", false);

        testStringParse(3, "1.11", "1", "1.", "1.1", false);
        testStringParse(3, "123.4", "1", "12", "123", false, false);
        testStringParse(3, "123.0", "1", "12", "123", false, false);
        testStringParse(3, "000123.0", "0", "0", "0", "1", "12", "123", false, false);
        testStringParse(3, "0.001", "0", "0.", "0.0", false, false);

    }

}
