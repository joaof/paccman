/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test class for % key.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcPercentTest extends CalcParserTestBase {

    public CalcPercentTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    @Test
    public void testPcNeedsOperator() {
        System.out.println("PcNeedsOperator");

        testStringParse("%", false);
        testStringParse("=%", "0", false);
        testStringParse("c%=", "0", false, "0");
        testStringParse("(%=", "0", false, "0");
        testStringParse("1+2=%", "1", "1", "2", "3", false);
        testStringParse("5%=", "5", false, "5");
        testStringParse("5%+2=", "5", false, "5", "2", "7");
    }

    @Test
    public void testPcWithOneOperatorOnly() {
        System.out.println("PcWithOneOperatorOnly");

        testStringParse("200+5%=", "2", "20", "200", "200", "5", "10", "210");
        testStringParse("200-7%=", "2", "20", "200", "200", "7", "14", "186");
        testStringParse("200x7%=", "2", "20", "200", "200", "7", "14", "2800");
        testStringParse(6, "200/7%=", "2", "20", "200", "200", "7", "14", "14.286");
    }

    @Test
    public void testOverflowOnEvaluatingPc() {
        System.out.println("OverflowOnEvaluationPc");

        testStringParse(3, "800+200%=", "8", "80", "800", "800", "2", "20", "200",
                ExprParser.FatalError.OVERFLOW, false);
    }

    @Test
    public void testPcWithMissingOperand() {
        System.out.println("PcWithMissingOperand");

        testStringParse("25+%=", "2", "25", "25", "6.25" /* 25% */, "31.25");
        testStringParse("25-%=", "2", "25", "25", "6.25" /* 25% */, "18.75");
        testStringParse("25x%=", "2", "25", "25", "6.25" /* 25% */, "156.25");
        testStringParse("25/%=", "2", "25", "25", "6.25" /* 25% */, "4");

        testStringParse("+25%=", "0", "2", "25", "0", "0");
        testStringParse("-25%=", "0", "2", "25", "0", "0");
        testStringParse("x25%=", "0", "2", "25", "0", "0");
        testStringParse("/25%=", "0", "2", "25", "0", ExprParser.FatalError.DIV_BY_ZERO);

    }

    @Test
    public void testPcWithinParen() {
        System.out.println("PcWithinParen");

        testStringParse("11+(100+5%)=", "1", "11", "11", "11", "1", "10", "100",
                "100", "5", "5", "105", "116");
        testStringParse("11+(100x5%)=", "1", "11", "11", "11", "1", "10", "100",
                "100", "5", "5", "500", "511");
        testStringParse("11+(100-5%)=", "1", "11", "11", "11", "1", "10", "100",
                "100", "5", "5", "95", "106");
        testStringParse("11+(100/5%)=", "1", "11", "11", "11", "1", "10", "100",
                "100", "5", "5", "20", "31");
    }

    @Test
    public void testPcAfterParen() {
        System.out.println("PcAfterParen");

        testStringParse("200+(2+3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "5", "10", "210");
        testStringParse("200+(2x3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "6", "12", "212");
        testStringParse("200+(2-3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "-1", "-2", "198");
        testStringParse(4, "200+(2/3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "0.67", "1.34", "201");

        testStringParse("200-(2+3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "5", "10", "190");
        testStringParse("200-(2x3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "6", "12", "188");
        testStringParse("200-(2-3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "-1", "-2", "202");
        testStringParse(4, "200-(2/3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "0.67", "1.34", "199");

        testStringParse("200x(2+3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "5", "10", "2000");
        testStringParse("200x(2x3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "6", "12", "2400");
        testStringParse("200x(2-3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "-1", "-2", "-400");
        testStringParse(4, "200x(2/3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "0.67", "1.34", "268");

        testStringParse("200/(2+3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "5", "10", "20");
        testStringParse(4, "200/(2x3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "6", "12", "16.7");
        testStringParse("200/(2-3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "-1", "-2", "-100");
        testStringParse(4, "200/(2/3)%=", "2", "20", "200", "200", "200", "2", "2",
                "3", "0.67", "1.34", "149");

    }

    @Test
    public void testPcPriority() {
        System.out.println("PcPriority");

        testStringParse("1+200x5%+3=", "1", "1", "2", "20", "200", "200", "5",
                "10", "2001", "3", "2004");
        testStringParse("3x4x5%=", "3", "3", "4", "12", "5", "0.6", "7.2");
    }

    @Test
    public void testPcWithCe() {
        System.out.println("PcWithCe");

        testStringParse("12+15c25%=", "1", "12", "12", "1", "15", "0", "2",
                "25", "3", "15");
        testStringParse("12+c%=", "1", "12", "12", "0", "0", "12");
        testStringParse("10+%c=", "1", "10", "10", "1" /* {10}% of 10 */, "0", "10");
        testStringParse("12+25%c=", "1", "12", "12", "2", "25", "3",
                "0", "12");
    }

    @Test
    public void testBadExprWithPc() {
        System.out.println("BadExprWithPc");

        testStringParse("1+(%)=", "1", "1", "1", false, "1", "2");
        testStringParse("1+2x(%)=", "1", "1", "2", "2", "2", false, "2", "5");
        testStringParse("1+(25%)=", "1", "1", "1", "2", "25", false, "25", "26");
        testStringParse("100+5%12=", "1", "10", "100", "100",
                "5", "5", false, false, "105");
        testStringParse("100+5%(=", "1", "10", "100", "100",
                "5", "5", false, "105");
    }

}
