/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for tesing EVAL key after one number enteering.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcEvalOneNumberParseTest extends CalcParserTestBase {

    public CalcEvalOneNumberParseTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    /**
     * Test of default display (no input from user)
     */
    @Test
    public void testNoEntry() {
        System.out.println("NoEntry");

        testStringParse("=", "0");
    }

    /**
     * Test single number input
     */
    @Test
    public void testNumberEval() {
        System.out.println("NumberEval");

        testStringParse("s=", "-0", "0");
        testStringParse("1=", "1", "1");
        testStringParse("1s=", "1", "-1", "-1");
        testStringParse("12=", "1", "12", "12");
        testStringParse("1.2=", "1", "1.", "1.2", "1.2");
        testStringParse("01.2=", "0", "1", "1.", "1.2", "1.2");
        testStringParse("01.2s3=", "0", "1", "1.", "1.2", "-1.2", "-1.23", "-1.23");
    }

    /**
     * Test numbers with trailing zeros in decimal part.
     */
    @Test
    public void testRemovalOfTrailingZeros() {
        System.out.println("RemovalOfTrailingZeros");
        
        testStringParse("10=", "1", "10", "10");
        testStringParse("1.=", "1", "1.", "1");
        testStringParse("1.0=", "1", "1.", "1.0", "1");
        testStringParse("1.00=", "1", "1.", "1.0", "1.00", "1");
        testStringParse("s=", "-0", "0");
        testStringParse("s0=", "-0", "-0", "0");
        testStringParse("1s.2300=", "1", "-1", "-1.", "-1.2", "-1.23", "-1.230", "-1.2300", "-1.23");
        testStringParse("10=", "1", "10", "10");
        testStringParse("1.2003=", "1", "1.", "1.2", "1.20", "1.200", "1.2003", "1.2003");
        testStringParse("1.20030=", "1", "1.", "1.2", "1.20", "1.200", "1.2003", "1.20030", "1.2003");
        testStringParse("01.203=", "0", "1", "1.", "1.2", "1.20", "1.203", "1.203");
        testStringParse("01.2030=", "0", "1", "1.", "1.2", "1.20", "1.203", "1.2030", "1.203");
        testStringParse("010.2030=", "0", "1", "10", "10.", "10.2", "10.20", "10.203", "10.2030", "10.203");
        testStringParse("010.20s30=", "0", "1", "10", "10.", "10.2", "10.20", "-10.20", "-10.203", "-10.2030", "-10.203");
        testStringParse("01.20.30=", "0", "1", "1.", "1.2", "1.20", false, "1.203", "1.2030", "1.203");
        testStringParse("0.00=", "0", "0.", "0.0", "0.00", "0");
    }

}
