/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Class for testing evaluation of two numbers (no operation)
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcEvalTwoNumbersTest extends CalcParserTestBase {

    public CalcEvalTwoNumbersTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUp() {
    }

    @AfterMethod
    public void tearDown() {
    }

    /**
     * Two integers.
     */
    @Test
    public void testEvalTwoNumbers() {
        System.out.println("EvalTwoNumbers");
        
        testStringParse("=1=", "0", "1", "1");
        testStringParse("1=1=", "1", "1", "1", "1");
        testStringParse("1=12=", "1", "1", "1", "12", "12");
        testStringParse("1=012=", "1", "1", "0", "1", "12", "12");
        testStringParse("0=12=", "0", "0", "1", "12", "12");
        testStringParse("012=1=", "0", "1", "12", "12", "1", "1");
        testStringParse("012=034=", "0", "1", "12", "12", "0", "3", "34", "34");
        testStringParse("1.23=45=", "1", "1.", "1.2", "1.23", "1.23", "4", "45",
                "45");
        testStringParse("45=1.2=", "4", "45", "45", "1", "1.", "1.2", "1.2");
        testStringParse("9.8=1.2=", "9", "9.", "9.8", "9.8", "1", "1.", "1.2",
                "1.2");
        testStringParse("9.s8=1.2=", "9", "9.", "-9.", "-9.8", "-9.8", "1", "1.",
                "1.2", "1.2");
        testStringParse("9.8=1.2s3=", "9", "9.", "9.8", "9.8", "1", "1.", "1.2",
                "-1.2", "-1.23", "-1.23");
        testStringParse("9.8=1.2s30=", "9", "9.", "9.8", "9.8", "1", "1.", "1.2",
                "-1.2", "-1.23", "-1.230", "-1.23");
        testStringParse("9.8s=1.23s0=", "9", "9.", "9.8", "-9.8", "-9.8", "1",
                "1.", "1.2", "1.23", "-1.23", "-1.230", "-1.23");
        testStringParse("s1=3=4s.2=6=", "-0", "-1", "-1", "3", "3", "4", "-4",
                "-4.", "-4.2", "-4.2", "6", "6");
    }

}
