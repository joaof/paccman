/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.calculator.impl.parser;

import java.math.BigDecimal;
import java.util.LinkedList;

/**
 * Parser class for the calculator.
 * @author joaof <jsc372@moonstice.com>
 */
public class CalcParser {

    private static final int MAX_PRECISION = 128;

    ExprParser exprParser;

    private boolean negative;

    private StringBuilder currentIntegerDisplay;

    private StringBuilder currentDecimalDisplay;

    private boolean parsingNumber;

    private boolean parsingDecimal;

    private boolean parseNumberOnCe;

    private boolean idle;

    private boolean clearEntry() {
        startNumberParse(true);
        return true;
    }

    private boolean parseBackspace() {
        assert parsingNumber || idle;

        if (idle) {
            return true;
        }
        if (parsingDecimal) {
            parseDecBackspace();
        } else {
            parseIntBackspace();
        }
        return true;
    }

    private void parseIntBackspace() {
        if (currentIntegerDisplay.length() == 1) {
            currentIntegerDisplay.replace(0, 1, "0");
        } else {
            currentIntegerDisplay.deleteCharAt(currentIntegerDisplay.length() - 1);
        }
    }

    private void parseDecBackspace() {
        if (currentDecimalDisplay.length() > 0) {
            currentDecimalDisplay.deleteCharAt(currentDecimalDisplay.length() - 1);
        } else {
            parsingDecimal = false;
        }
    }

    private boolean parseNumberToken(CalcToken token) {
        if (!parsingNumber) {
            startNumberParse(false);
        }
        if (token.isDigit()) {
            return parseDigit(token);
        } else if (token == CalcToken.POINT) {
            return parsePoint();
        } else if (token == CalcToken.SIGN) {
            return parseSign();
        } else {
            throw new IllegalStateException("Invalid character passed to number pass fucntion");
        }
    }

    private void startNumberParse(boolean onCe) {
        parsingNumber = true;
        parsingDecimal = false;
        currentIntegerDisplay = new StringBuilder("0");
        currentDecimalDisplay = new StringBuilder();
        negative = false;
        parseNumberOnCe = onCe;
    }

    private void endNumberParse() {
        if (parsingNumber) {
            exprParser.parseToken(ExprToken.NUMBER, buildCurrentNumber());
            parsingNumber = false;
        }
    }

    public final void reset() {
        exprParser.reset();
        parsingNumber = parsingDecimal = false;
        negative = false;
        idle = true;
    }

    CalcToken previousToken;

    boolean isValidToken(CalcToken token) {
        if (exprParser.getFatalError() != null && token != CalcToken.RESET) {
            return false;
        }

        if (token == CalcToken.CLOSE_PAREN && exprParser.parenLevel == 0) {
            return false;
        }
        if (token == CalcToken.OPEN_PAREN && parsingNumber && !parseNumberOnCe) {
            return false;
        }
        if (token == CalcToken.POINT && parsingDecimal) {
            return false;
        }
        if (token == CalcToken.BACKSPACE && !(parsingNumber || idle)) {
            return false;
        }
        if (previousToken == CalcToken.CLOSE_PAREN) {
            if (token.isNumberCharacter()) {
                return false;
            }
            if (token == CalcToken.OPEN_PAREN) {
                return false;
            }
        }
        if (previousToken == CalcToken.PC) {
            if (token.isNumberCharacter()) {
                return false;
            }
            if (token == CalcToken.OPEN_PAREN) {
                return false;
            }
        }
        return true;
    }

    private String getLastInputs() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < lastTokens.size(); i++) {
            CalcToken token = lastTokens.get(i);
            s.append("\t");
            s.append(token);
            s.append("\t");
            s.append(token.getChar());
            if (i != lastTokens.size() - 1) {
                s.append("\n");
            }
        }
        return s.toString();
    }

    /**
     * Parse a key input. Returns {@code true} if the key is valid, {@code false}
     * if the key is not valid.
     * Note: the only valid key when in fatal error is RESET.
     * @param c
     * @return
     */
    public boolean parseChar(char in) {
        try {
            return doParseChar(in);
        } catch (Throwable t) {
            throw new IllegalStateException("Exception parsing char. Last inputs (last first):\n" + getLastInputs(), t);
        }
    }

    private boolean doParseChar(char in) {
        boolean result;
        CalcToken token = CalcToken.getToken(in);
        lastTokens.addFirst(token);
        if (!isValidToken(token)) {
            return false;
        }

        if (token == CalcToken.RESET) {
            reset();
            result = true;
        } else {
            if (token.isNumberCharacter()) {
                result = parseNumberToken(token);
            } else if (token == CalcToken.CE) {
                result = clearEntry();
            } else {
                switch (token) {
                    case EVAL:
                        endNumberParse();
                        result = exprParser.parseToken(ExprToken.EVAL);
                        break;

                    case PLUS:
                    case MINUS:
                    case MULT:
                    case DIV:
                        endNumberParse();
                        result = exprParser.parseToken(ExprToken.OPERATOR, token);
                        break;

                    case OPEN_PAREN:
                        result = exprParser.parseToken(ExprToken.OPEN_PAREN);
                        break;

                    case CLOSE_PAREN:
                        endNumberParse();
                        result = exprParser.parseToken(ExprToken.CLOSE_PAREN);
                        break;

                    case PC:
                        endNumberParse();
                        result = exprParser.parseToken(ExprToken.PC);
                        break;

                    case BACKSPACE:
                        result = parseBackspace();
                        break;

                    default:
                        throw new UnsupportedOperationException("Not yet implemented");
                }
            }
        }

        if (result && token != CalcToken.RESET) {
            idle = false;
            previousToken = token;
        }
        return result;
    }

    boolean appendDigitToIntPart(char c) {
        if (currentIntegerDisplay.toString().equals("0")) {
            currentIntegerDisplay = new StringBuilder();
        }
        if (currentIntegerDisplay.length() >= maxPrecision) {
            return false;
        }
        currentIntegerDisplay.append(c);
        return true;
    }

    boolean appendDigitToDecPart(char c) {
        if (currentIntegerDisplay.length() + 1 + currentDecimalDisplay.length() >= maxPrecision) {
            return false;
        }
        currentDecimalDisplay.append(c);
        return true;
    }

    /**
     * Build the string to be displayed when the user is typing a number.
     * @see #getDisplay() 
     * @return
     */
    private String getCurrentDisplay() {
        String display = negative ? "-" : "";
        display += currentIntegerDisplay;
        if (parsingDecimal) {
            display += '.';
            display += currentDecimalDisplay;
        }
        return display;
    }

    /**
     * Parse a digit key.
     * @param c
     * @return
     */
    private boolean parseDigit(CalcToken token) {
        if (parsingDecimal) {
            return appendDigitToDecPart(token.c);
        } else {
            return appendDigitToIntPart(token.c);
        }
    }

    private boolean parsePoint() {
        if (currentIntegerDisplay.length() + 1 >= maxPrecision) {
            // Decimal part will not fit the display
            return false;
        }
        parsingDecimal = true;
        return true;
    }

    private boolean parseSign() {
        negative = !negative;
        return true;
    }

    /**
     * Get the string to be displayed.
     * @return
     */
    public String getDisplay() {
        if (parsingNumber) {
            return getCurrentDisplay();
        } else {
            return exprParser.getRegister().toPlainString();
        }
    }

    /**
     * The maximum number of digit the calculator can display (including decimal
     * point but not the negative sign)
     */
    private final int maxPrecision;

    /**
     * 
     * @param maxPrecision The number of characters for display: the user can not 
     * enter a number with more than {@code maxPrecision} characters (including decimal point,
     * sign is not counted) If a result display does not fit this size, it is fatal
     * error.
     */
    public CalcParser(int maxPrecision) {
        if (maxPrecision < 1 && maxPrecision > MAX_PRECISION) {
            throw new IllegalArgumentException("maxPrecision must be > 0 and < "
                    + Integer.toString(MAX_PRECISION));
        }
        this.maxPrecision = maxPrecision;
        exprParser = new ExprParser(maxPrecision);
        reset();
    }

    public CalcParser() {
        this(MAX_PRECISION);
    }

    private BigDecimal buildCurrentNumber() {
        return new BigDecimal(getDisplay());
    }

    public ExprParser.FatalError getFatalError() {
        return exprParser.fatalError;
    }

    private final LinkedList<CalcToken> lastTokens = new LinkedList<CalcToken>() {

        public static final int TOKEN_FIFO_SIZE = 100;

        @Override
        public void addFirst(CalcToken e) {
            if (size() == TOKEN_FIFO_SIZE) {
                removeLast();
            }
            super.addFirst(e);
        }

    };

}
