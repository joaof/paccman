--
-- Generated on 2014-12-05 00:09:26.426160 from MySQL Workbench using mysql2derby_plugin, version 10.
--

--
-- Constraints creation
--

ALTER TABLE ObjAccounts ADD CONSTRAINT fk_ObjAccounts_reconciliationStatus_RECONCILIATE_STATUS
FOREIGN KEY(reconciliationStatus) REFERENCES ENUM_RECONCILIATE_STATUS(value);

ALTER TABLE ObjTransactions ADD CONSTRAINT fk_ObjTransactions_reconciliationStatus_RECONCILIATE_STATUS
FOREIGN KEY(reconciliationStatus) REFERENCES ENUM_RECONCILIATE_STATUS(value);

ALTER TABLE ObjScheduledTransactions ADD CONSTRAINT fk_ObjScheduledTransactions_periodUnit_PERIOD_UNIT
FOREIGN KEY(periodUnit) REFERENCES ENUM_PERIOD_UNIT(value);

ALTER TABLE ObjAddresses ADD CONSTRAINT fk_ObjAdresses_RefCountries1
FOREIGN KEY(idCountry) REFERENCES RefCountries(codeISO3166);

ALTER TABLE ObjPersons ADD CONSTRAINT fk_ObjPersons_ObjAdresses1
FOREIGN KEY(idAddress) REFERENCES ObjAddresses(idAddress);

ALTER TABLE ObjPersons ADD CONSTRAINT fk_ObjPersons_ObjInfos1
FOREIGN KEY(idInfo) REFERENCES ObjInfos(idInfo);

ALTER TABLE ObjBanks ADD CONSTRAINT fk_ObjBanks_ObjInfos1
FOREIGN KEY(idBankInfo) REFERENCES ObjInfos(idInfo);

ALTER TABLE ObjBanks ADD CONSTRAINT fk_ObjBanks_ObjAdresses1
FOREIGN KEY(idBankAddress) REFERENCES ObjAddresses(idAddress);

ALTER TABLE ObjBanks ADD CONSTRAINT fk_ObjBanks_ObjPersons1
FOREIGN KEY(idBanker) REFERENCES ObjPersons(idPerson);

ALTER TABLE ObjAccounts ADD CONSTRAINT fk_ObjAccounts_ObjBanks1
FOREIGN KEY(idBank) REFERENCES ObjBanks(idBank);

ALTER TABLE ObjAccounts ADD CONSTRAINT fk_ObjAccounts_ObjAccountTypes1
FOREIGN KEY(idAccountType) REFERENCES ObjAccountTypes(idAccountType);

ALTER TABLE ObjAccounts ADD CONSTRAINT fk_ObjAccounts_ObjPersons1
FOREIGN KEY(idAccountHolder) REFERENCES ObjPersons(idPerson);

ALTER TABLE ObjTransactions ADD CONSTRAINT fkTransactionAccount
FOREIGN KEY(idAccount) REFERENCES ObjAccounts(idAccount);

ALTER TABLE ObjTransactions ADD CONSTRAINT fkTransactionPaiementMode
FOREIGN KEY(idPaiementMode) REFERENCES ObjPaiementModes(idPaiementMode);

ALTER TABLE ObjTransactions ADD CONSTRAINT fkTransactionPayee
FOREIGN KEY(idPayee) REFERENCES ObjPayees(idPayee);

ALTER TABLE ObjTransactionEntries ADD CONSTRAINT fkTransactionEntryCategory
FOREIGN KEY(idCategory) REFERENCES ObjCategories(idCategory);

ALTER TABLE ObjTransactionEntries ADD CONSTRAINT fkTransactionEntryTransaction
FOREIGN KEY(idTransaction) REFERENCES ObjTransactions(idTransaction);

ALTER TABLE ObjCategories ADD CONSTRAINT fkCategoryCategoryGroup
FOREIGN KEY(idCategoryGroup) REFERENCES ObjCategoryGroups(idCategoryGroup);

ALTER TABLE ObjTransfers ADD CONSTRAINT fkTransferTransactionEntry
FOREIGN KEY(idTransactionEntry) REFERENCES ObjTransactionEntries(idTransactionEntry);

ALTER TABLE ObjTransfers ADD CONSTRAINT fkTransferTransfer
FOREIGN KEY(idOtherTransactionEntry) REFERENCES ObjTransfers(idTransactionEntry);

ALTER TABLE AssoCategoriesTransactionModels ADD CONSTRAINT fk_ObjCategories_has_ObjTransactionModel_ObjCategories1
FOREIGN KEY(idCategory) REFERENCES ObjCategories(idCategory);

ALTER TABLE AssoCategoriesTransactionModels ADD CONSTRAINT fk_ObjCategories_has_ObjTransactionModel_ObjTransactionModel1
FOREIGN KEY(idTransactionModel) REFERENCES ObjTransactionModels(idTransactionModel);

ALTER TABLE ObjScheduledTransactions ADD CONSTRAINT fk_ObjScheduledTransactions_ObjTransactions1
FOREIGN KEY(idTransactionId) REFERENCES ObjTransactions(idTransaction);

ALTER TABLE AssoTransactionsScheduledTransactions ADD CONSTRAINT fk_ObjTransactions_has_ObjScheduledTransactions_ObjTransactio1
FOREIGN KEY(idTransaction) REFERENCES ObjTransactions(idTransaction);

ALTER TABLE AssoTransactionsScheduledTransactions ADD CONSTRAINT fk_ObjTransactions_has_ObjScheduledTransactions_ObjScheduledT1
FOREIGN KEY(idScheduledTransactions) REFERENCES ObjScheduledTransactions(idTransactionId);

CREATE UNIQUE INDEX UIX_ObjAccountTypes_UNIQUE_NAME ON ObjAccountTypes(name);

CREATE UNIQUE INDEX UIX_ObjBanks_UNIQUE_NAME ON ObjBanks(bankName);

CREATE UNIQUE INDEX UIX_ObjAccounts_UNIQUE_NAME ON ObjAccounts(name);

CREATE UNIQUE INDEX UIX_ObjPaiementModes_UNIQUE_NAME ON ObjPaiementModes(paiementModeName);

CREATE UNIQUE INDEX UIX_ObjCategories_UNIQUE_NAME ON ObjCategories(categoryName);

CREATE UNIQUE INDEX UIX_ObjCategoryGroups_UNIQUE_NAME ON ObjCategoryGroups(categoryGroupName);

CREATE UNIQUE INDEX UIX_ObjTransactionModels_UNIQUE_NAME ON ObjTransactionModels(transactionModelName);

CREATE UNIQUE INDEX UIX_ObjPayees_UNIQUE_NAME ON ObjPayees(name);

CREATE UNIQUE INDEX UIX_AssoTransactionsScheduledTransactions_idTransaction_UNIQUE ON AssoTransactionsScheduledTransactions(idTransaction);

