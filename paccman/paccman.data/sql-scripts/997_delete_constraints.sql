--
-- Generated on 2014-12-05 00:09:26.426279 from MySQL Workbench using mysql2derby_plugin, version 10.
--

--
-- Constraints deletion
--

ALTER TABLE ObjAddresses DROP FOREIGN KEY fk_ObjAdresses_RefCountries1;

ALTER TABLE ObjPersons DROP FOREIGN KEY fk_ObjPersons_ObjAdresses1;

ALTER TABLE ObjPersons DROP FOREIGN KEY fk_ObjPersons_ObjInfos1;

ALTER TABLE ObjBanks DROP FOREIGN KEY fk_ObjBanks_ObjInfos1;

ALTER TABLE ObjBanks DROP FOREIGN KEY fk_ObjBanks_ObjAdresses1;

ALTER TABLE ObjBanks DROP FOREIGN KEY fk_ObjBanks_ObjPersons1;

ALTER TABLE ObjAccounts DROP FOREIGN KEY fk_ObjAccounts_ObjBanks1;

ALTER TABLE ObjAccounts DROP FOREIGN KEY fk_ObjAccounts_ObjAccountTypes1;

ALTER TABLE ObjAccounts DROP FOREIGN KEY fk_ObjAccounts_ObjPersons1;

ALTER TABLE ObjTransactions DROP FOREIGN KEY fkTransactionAccount;

ALTER TABLE ObjTransactions DROP FOREIGN KEY fkTransactionPaiementMode;

ALTER TABLE ObjTransactions DROP FOREIGN KEY fkTransactionPayee;

ALTER TABLE ObjTransactionEntries DROP FOREIGN KEY fkTransactionEntryCategory;

ALTER TABLE ObjTransactionEntries DROP FOREIGN KEY fkTransactionEntryTransaction;

ALTER TABLE ObjCategories DROP FOREIGN KEY fkCategoryCategoryGroup;

ALTER TABLE ObjTransfers DROP FOREIGN KEY fkTransferTransactionEntry;

ALTER TABLE ObjTransfers DROP FOREIGN KEY fkTransferTransfer;

ALTER TABLE AssoCategoriesTransactionModels DROP FOREIGN KEY fk_ObjCategories_has_ObjTransactionModel_ObjCategories1;

ALTER TABLE AssoCategoriesTransactionModels DROP FOREIGN KEY fk_ObjCategories_has_ObjTransactionModel_ObjTransactionModel1;

ALTER TABLE ObjScheduledTransactions DROP FOREIGN KEY fk_ObjScheduledTransactions_ObjTransactions1;

ALTER TABLE AssoTransactionsScheduledTransactions DROP FOREIGN KEY fk_ObjTransactions_has_ObjScheduledTransactions_ObjTransactio1;

ALTER TABLE AssoTransactionsScheduledTransactions DROP FOREIGN KEY fk_ObjTransactions_has_ObjScheduledTransactions_ObjScheduledT1;

DROP INDEX UIX_ObjAccountTypes_UNIQUE_NAME;

DROP INDEX UIX_ObjBanks_UNIQUE_NAME;

DROP INDEX UIX_ObjAccounts_UNIQUE_NAME;

DROP INDEX UIX_ObjPaiementModes_UNIQUE_NAME;

DROP INDEX UIX_ObjCategories_UNIQUE_NAME;

DROP INDEX UIX_ObjCategoryGroups_UNIQUE_NAME;

DROP INDEX UIX_ObjTransactionModels_UNIQUE_NAME;

DROP INDEX UIX_ObjPayees_UNIQUE_NAME;

DROP INDEX UIX_AssoTransactionsScheduledTransactions_idTransaction_UNIQUE;

