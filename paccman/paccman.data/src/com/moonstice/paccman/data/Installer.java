/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import com.moonstice.paccman.data.persistence.PersistInfo;
import com.moonstice.paccman.data.persistence.PersistService;
import com.moonstice.paccman.data.persistence.PersistStorage;
import org.netbeans.api.progress.BaseProgressUtils;
import org.openide.LifecycleManager;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.InstalledFileLocator;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public class Installer extends ModuleInstall {

    @Override
    public void restored() {
        WindowManager.getDefault().invokeWhenUIReady(this::onUiReady);
    }

    @Override
    public boolean closing() {
        try {
            PersistService pserv = Lookup.getDefault().lookup(PersistService.class);
            final Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT")); // NOI18N
            PersistInfo info = pserv.getPersistInfo();
            info.setLastAccessDate(now);
            pserv.writePersistInfo(info);
            Lookup.getDefault().lookup(PersistStorage.class).close();
        } catch (Exception ex) {
            Exceptions.printStackTrace(ex);
        }
        return true;
    }

    public static final String DATA_PATH = "paccdata";

    public static final String DB_PATH = "db";

    public void setInitFailed(Exception initFailed) {
        this.initFailed = initFailed;
    }

    private Exception initFailed = null;

    private void onUiReady() {
        BaseProgressUtils.runOffEventDispatchThread(() -> {
            try {
                boolean newDb = false;
                if (FileUtil.getConfigRoot().getFileObject(DATA_PATH) == null) {
                    installDatabase();
                    newDb = true;
                }
                PersistStorage pstor = Lookup.getDefault().lookup(PersistStorage.class);
                pstor.open(getDatabasePath());
                PersistService pserv = Lookup.getDefault().lookup(PersistService.class);
                pserv.init(pstor);
                if (newDb) {
                    PersistInfo info = new PersistInfo();
                    final Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT")); // NOI18N
                    info.setCreationDate(now);
                    info.setLastUpdateDate(now);
                    info.setLastAccessDate(now);
                    info.setName("DATABASE"); // NOI18N
                    pserv.writePersistInfo(info);
                }
            } catch (Exception ex) {
                setInitFailed(ex);
            }
        }, NbBundle.getMessage(Installer.class, "init.storage"), new AtomicBoolean(), false);
        if (initFailed != null) {
            Exceptions.printStackTrace(initFailed);
            LifecycleManager.getDefault().exit();
        }
    }

    private String getDatabasePath() {
        return FileUtil.toFile(FileUtil.getConfigFile(DATA_PATH + "/" + DB_PATH)).getAbsolutePath();
    }

    private void installDatabase() {
        try {
            File zipFile = InstalledFileLocator.getDefault().locate("template.paccmandb.zip", "com.moonstice.paccman.data", false);
            ZipFile zf = new ZipFile(zipFile);
            FileObject fo = FileUtil.getConfigRoot().createFolder(DATA_PATH);
            fo = fo.createFolder(DB_PATH);
            unzipFileIntoDirectory(zf, FileUtil.toFile(fo));
        } catch (ZipException ex) {
            Exceptions.printStackTrace(ex);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    public static void unzipFileIntoDirectory(ZipFile zipFile, File toDir) throws IOException {
        Enumeration<? extends ZipEntry> files = zipFile.entries();
        File f;
        FileOutputStream fos = null;

        try {
            while (files.hasMoreElements()) {
                ZipEntry entry = (ZipEntry) files.nextElement();
                InputStream eis = zipFile.getInputStream(entry);
                byte[] buffer = new byte[1024];
                int bytesRead;

                f = new File(toDir.getAbsolutePath() + File.separator + entry.getName());

                if (entry.isDirectory()) {
                    f.mkdirs();
                    continue;
                } else {
                    f.getParentFile().mkdirs();
                    f.createNewFile();
                }

                fos = new FileOutputStream(f);

                while ((bytesRead = eis.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
            }
        } catch (IOException ex) {
            throw new IOException(ex);
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
    }

}
