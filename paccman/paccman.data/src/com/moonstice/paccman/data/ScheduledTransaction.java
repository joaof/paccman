/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011-2012, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJSCHEDULEDTRANSACTIONS")
@NamedQueries({
    @NamedQuery(name = "ScheduledTransaction.findAll", query = "SELECT s FROM ScheduledTransaction s"),
    @NamedQuery(name = "ScheduledTransaction.findByIdtransactionid", query = "SELECT s FROM ScheduledTransaction s WHERE s.idtransactionid = :idtransactionid"),
    @NamedQuery(name = "ScheduledTransaction.findByShortdescription", query = "SELECT s FROM ScheduledTransaction s WHERE s.shortdescription = :shortdescription"),
    @NamedQuery(name = "ScheduledTransaction.findByNextoccurrence", query = "SELECT s FROM ScheduledTransaction s WHERE s.nextoccurrence = :nextoccurrence"),
    @NamedQuery(name = "ScheduledTransaction.findByPeriod", query = "SELECT s FROM ScheduledTransaction s WHERE s.period = :period"),
    @NamedQuery(name = "ScheduledTransaction.findByPeriodunit", query = "SELECT s FROM ScheduledTransaction s WHERE s.periodunit = :periodunit"),
    @NamedQuery(name = "ScheduledTransaction.findByDaysbeforenotification", query = "SELECT s FROM ScheduledTransaction s WHERE s.daysbeforenotification = :daysbeforenotification"),
    @NamedQuery(name = "ScheduledTransaction.findByIsautomatic", query = "SELECT s FROM ScheduledTransaction s WHERE s.isautomatic = :isautomatic"),
    @NamedQuery(name = "ScheduledTransaction.findByIsfixedamount", query = "SELECT s FROM ScheduledTransaction s WHERE s.isfixedamount = :isfixedamount")})
public class ScheduledTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @Column(name = "IDTRANSACTIONID")
    private Integer idtransactionid;

    @Basic(optional = false)
    @Column(name = "SHORTDESCRIPTION")
    private String shortdescription;

    @Basic(optional = false)
    @Column(name = "NEXTOCCURRENCE")
    @Temporal(TemporalType.DATE)
    private Date nextoccurrence;

    @Basic(optional = false)
    @Column(name = "PERIOD")
    private int period;

    @Basic(optional = false)
    @Column(name = "PERIODUNIT")
    private String periodunit;

    @Basic(optional = false)
    @Column(name = "DAYSBEFORENOTIFICATION")
    private int daysbeforenotification;

    @Basic(optional = false)
    @Column(name = "ISAUTOMATIC")
    private short isautomatic;

    @Basic(optional = false)
    @Column(name = "ISFIXEDAMOUNT")
    private short isfixedamount;

    @JoinTable(name = "ASSOTRANSACTIONSSCHEDULEDTRANSACTIONS", joinColumns = {
        @JoinColumn(name = "IDSCHEDULEDTRANSACTION", referencedColumnName = "IDTRANSACTIONID")}, inverseJoinColumns = {
        @JoinColumn(name = "IDTRANSACTION", referencedColumnName = "IDTRANSACTION")})
    @ManyToMany
    private Collection<Transaction> transactionCollection;

    @JoinColumn(name = "IDTRANSACTIONID", referencedColumnName = "IDTRANSACTION", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Transaction transaction;

    public ScheduledTransaction() {
    }

    public ScheduledTransaction(Integer idtransactionid) {
        this.idtransactionid = idtransactionid;
    }

    public ScheduledTransaction(Integer idtransactionid, String shortdescription, Date nextoccurrence, int period, String periodunit, int daysbeforenotification, short isautomatic, short isfixedamount) {
        this.idtransactionid = idtransactionid;
        this.shortdescription = shortdescription;
        this.nextoccurrence = nextoccurrence;
        this.period = period;
        this.periodunit = periodunit;
        this.daysbeforenotification = daysbeforenotification;
        this.isautomatic = isautomatic;
        this.isfixedamount = isfixedamount;
    }

    public Integer getIdtransactionid() {
        return idtransactionid;
    }

    public void setIdtransactionid(Integer idtransactionid) {
        this.idtransactionid = idtransactionid;
    }

    public String getShortdescription() {
        return shortdescription;
    }

    public void setShortdescription(String shortdescription) {
        this.shortdescription = shortdescription;
    }

    public Date getNextoccurrence() {
        return nextoccurrence;
    }

    public void setNextoccurrence(Date nextoccurrence) {
        this.nextoccurrence = nextoccurrence;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getPeriodunit() {
        return periodunit;
    }

    public void setPeriodunit(String periodunit) {
        this.periodunit = periodunit;
    }

    public int getDaysbeforenotification() {
        return daysbeforenotification;
    }

    public void setDaysbeforenotification(int daysbeforenotification) {
        this.daysbeforenotification = daysbeforenotification;
    }

    public short getIsautomatic() {
        return isautomatic;
    }

    public void setIsautomatic(short isautomatic) {
        this.isautomatic = isautomatic;
    }

    public short getIsfixedamount() {
        return isfixedamount;
    }

    public void setIsfixedamount(short isfixedamount) {
        this.isfixedamount = isfixedamount;
    }

    public Collection<Transaction> getTransactionCollection() {
        return transactionCollection;
    }

    public void setTransactionCollection(Collection<Transaction> transactionCollection) {
        this.transactionCollection = transactionCollection;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtransactionid != null ? idtransactionid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ScheduledTransaction)) {
            return false;
        }
        ScheduledTransaction other = (ScheduledTransaction) object;
        if ((this.idtransactionid == null && other.idtransactionid != null) || (this.idtransactionid != null && !this.idtransactionid.equals(other.idtransactionid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.ScheduledTransaction[idtransactionid=" + idtransactionid + "]";
    }

}
