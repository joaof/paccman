/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJTRANSACTIONENTRIES")
@NamedQueries({
    @NamedQuery(name = "TransactionEntry.findAll", query = "SELECT t FROM TransactionEntry t"),
    @NamedQuery(name = "TransactionEntry.findByIdtransactionentry", query = "SELECT t FROM TransactionEntry t WHERE t.idtransactionentry = :idtransactionentry"),
    @NamedQuery(name = "TransactionEntry.findByAmount", query = "SELECT t FROM TransactionEntry t WHERE t.amount = :amount"),
    @NamedQuery(name = "TransactionEntry.findByNote", query = "SELECT t FROM TransactionEntry t WHERE t.note = :note")})
public class TransactionEntry implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDTRANSACTIONENTRY")
    private Integer idtransactionentry;

    @Basic(optional = false)
    @Column(name = "AMOUNT")
    private int amount;

    @Column(name = "NOTE")
    private String note;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "transactionEntry")
    private Transfer transfer;

    @JoinColumn(name = "IDTRANSACTION", referencedColumnName = "IDTRANSACTION")
    @ManyToOne(optional = false)
    private Transaction transaction;

    @JoinColumn(name = "IDCATEGORY", referencedColumnName = "IDCATEGORY")
    @ManyToOne(optional = false)
    private Category category;

    public TransactionEntry() {
    }

    public TransactionEntry(Integer idtransactionentry) {
        this.idtransactionentry = idtransactionentry;
    }

    public TransactionEntry(Integer idtransactionentry, int amount) {
        this.idtransactionentry = idtransactionentry;
        this.amount = amount;
    }

    public Integer getIdtransactionentry() {
        return idtransactionentry;
    }

    public void setIdtransactionentry(Integer idtransactionentry) {
        this.idtransactionentry = idtransactionentry;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtransactionentry != null ? idtransactionentry.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionEntry)) {
            return false;
        }
        TransactionEntry other = (TransactionEntry) object;
        if ((this.idtransactionentry == null && other.idtransactionentry != null) || (this.idtransactionentry != null && !this.idtransactionentry.equals(other.idtransactionentry))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.TransactionEntry[idtransactionentry=" + idtransactionentry + "]";
    }

}
