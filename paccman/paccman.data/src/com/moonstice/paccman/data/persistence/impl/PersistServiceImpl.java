/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data.persistence.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.moonstice.paccman.data.persistence.PersistInfo;
import com.moonstice.paccman.data.persistence.PersistService;
import com.moonstice.paccman.data.persistence.PersistStorage;
import com.moonstice.paccman.data.persistence.Persistable;
import org.openide.util.NbBundle;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@ServiceProvider(service = PersistService.class)
public class PersistServiceImpl implements PersistService {

    @Override
    public void persist(Persistable o) {
        if (!em.contains(o)) {
            em.persist(o);
        }
        em.flush();
        changed = true;
    }

    @Override
    public void saveAll() {
        if (!changed) {
            return;
        }
        em.getTransaction().commit();
        em.getTransaction().begin();
        changed = false;
    }

    @Override
    public boolean hasChanged() {
        return changed;
    }

    @Override
    public void init(PersistStorage storage) {
        String path = storage.getStoragePath();
        HashMap<String, String> hashMap = new HashMap<>(1);
        hashMap.put("javax.persistence.jdbc.url", "jdbc:derby:" + path); // NOI18N
        final EntityManagerFactory createEntityManagerFactory =
                Persistence.createEntityManagerFactory("paccmanPU", hashMap); //NOI18N
        em = createEntityManagerFactory.createEntityManager();
        em.getTransaction().begin();
    }

    private EntityManager em;

    private boolean changed = false;

    private final static String PROP_NAME = "Name"; // NOI18N

    private final static String PROP_CREATION_DATE = "CreationDate"; // NOI18N

    private static final String PROP_DBMODEL_VERSION = "ModelVersion"; // NOI18N

    private static final String PROP_LAST_UPDATE = "LastUpdate"; // NOI18N

    private static final String PROP_LAST_ACCESS = "LastAccess"; // NOI18N

    private void persistStringInfo(String propName, String propValue) {
        DatabaseInfo dbInfo = em.find(DatabaseInfo.class, propName);
        if (dbInfo == null) {
            dbInfo = new DatabaseInfo(propName, propValue);
        } else if (dbInfo.getInfovalue().equals(propValue)) {
            return;
        } else {
            dbInfo.setInfovalue(propValue);
        }
        persist(dbInfo);
    }

    private void persistCalendarInfo(String propName, Calendar date) {
        DatabaseInfo dbInfo = em.find(DatabaseInfo.class, propName);
        if (dbInfo == null) {
            dbInfo = new DatabaseInfo(propName, date);
        } else if (dbInfo.getCalendarInfoValue().equals(date)) {
            return;
        } else {
            dbInfo.setCalendarInfoValue(date);
        }
        persist(dbInfo);
    }

    @Override
    public void writePersistInfo(final PersistInfo info) {
        persistStringInfo(PROP_NAME, info.getName());
        persistCalendarInfo(PROP_CREATION_DATE, info.getCreationDate());
        persistCalendarInfo(PROP_LAST_UPDATE, info.getLastUpdateDate());
        persistCalendarInfo(PROP_LAST_ACCESS, info.getLastAccessDate());
        persistStringInfo(PROP_DBMODEL_VERSION,
                NbBundle.getMessage(PersistServiceImpl.class, "paccman.model.version"));
        saveAll();
    }

    private PersistInfo infos;

    @Override
    public PersistInfo getPersistInfo() {
        if (infos == null) {
            infos = new PersistInfo();
            DatabaseInfo info;
            info = em.find(DatabaseInfo.class, PROP_NAME);
            infos.setName(info.getInfovalue());
            info = em.find(DatabaseInfo.class, PROP_CREATION_DATE);
            infos.setCreationDate(info.getCalendarInfoValue());
            info = em.find(DatabaseInfo.class, PROP_LAST_UPDATE);
            infos.setLastUpdateDate(info.getCalendarInfoValue());
            info = em.find(DatabaseInfo.class, PROP_LAST_ACCESS);
            infos.setLastAccessDate(info.getCalendarInfoValue());
            info = em.find(DatabaseInfo.class, PROP_DBMODEL_VERSION);
            infos.setModelVersion(info.getInfovalue());
        }
        return infos;
    }

    @Override
    public <T extends Persistable> List<T> getAll(Class<T> clazz) {
        Query query = em.createNamedQuery(clazz.getSimpleName() + ".findAll");
        return query.getResultList();
    }

}
