/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data.persistence;

import java.util.List;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
public interface PersistService {

    /**
     * Persist the object.
     * @param p the Persistable to be persisted.
     */
    void persist(Persistable p);

    /**
     * Write the persisted objects to database (i.e. {@code commit})
     */
    void saveAll();

    /**
     * Indicates if at least an entity has been persisted (save needed).
     * @return {@code true} if a change has been made, {@code false} otherwise.
     */
    boolean hasChanged();

    /**
     * Initialize the persistence service.
     * @param pstor the persistence storage associated to the service.
     */
    void init(PersistStorage pstor);

    /**
     * Create or update the information of the persistence.
     * @param info Information concerning the persistence.
     */
    void writePersistInfo(PersistInfo info);

    /**
     * Get the persistence information.
     * @return the persistence information.
     */
    PersistInfo getPersistInfo();

    /**
     * Get all the persisted item of the specified class.
     * @param <T>
     * @param clazz
     * @return 
     */
    public <T extends Persistable> List<T> getAll(Class<T> clazz);

}
