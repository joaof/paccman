/*
 *
 * This file is part of PAccMan.
 *
 * PAccMan is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PAccMan is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PAccMan.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011, 2015 joaof <jsc372@moonstice.com>
 *
 */

package com.moonstice.paccman.data;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author joaof <jsc372@moonstice.com>
 */
@Entity
@Table(name = "OBJPAIEMENTMODES")
@NamedQueries({
    @NamedQuery(name = "PaiementMode.findAll", query = "SELECT p FROM PaiementMode p"),
    @NamedQuery(name = "PaiementMode.findByIdpaiementmode", query = "SELECT p FROM PaiementMode p WHERE p.idpaiementmode = :idpaiementmode"),
    @NamedQuery(name = "PaiementMode.findByPaiementmodename", query = "SELECT p FROM PaiementMode p WHERE p.paiementmodename = :paiementmodename")})
public class PaiementMode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IDPAIEMENTMODE")
    private Integer idpaiementmode;

    @Basic(optional = false)
    @Column(name = "PAIEMENTMODENAME")
    private String paiementmodename;

    @OneToMany(mappedBy = "paiementMode")
    private Collection<Transaction> transactionCollection;

    public PaiementMode() {
    }

    public PaiementMode(Integer idpaiementmode) {
        this.idpaiementmode = idpaiementmode;
    }

    public PaiementMode(Integer idpaiementmode, String paiementmodename) {
        this.idpaiementmode = idpaiementmode;
        this.paiementmodename = paiementmodename;
    }

    public Integer getIdpaiementmode() {
        return idpaiementmode;
    }

    public void setIdpaiementmode(Integer idpaiementmode) {
        this.idpaiementmode = idpaiementmode;
    }

    public String getPaiementmodename() {
        return paiementmodename;
    }

    public void setPaiementmodename(String paiementmodename) {
        this.paiementmodename = paiementmodename;
    }

    public Collection<Transaction> getTransactionCollection() {
        return transactionCollection;
    }

    public void setTransactionCollection(Collection<Transaction> transactionCollection) {
        this.transactionCollection = transactionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpaiementmode != null ? idpaiementmode.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaiementMode)) {
            return false;
        }
        PaiementMode other = (PaiementMode) object;
        if ((this.idpaiementmode == null && other.idpaiementmode != null) || (this.idpaiementmode != null && !this.idpaiementmode.equals(other.idpaiementmode))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.moonstice.paccman.data.PaiementMode[idpaiementmode=" + idpaiementmode + "]";
    }

}
